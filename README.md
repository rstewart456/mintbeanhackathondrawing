# MintBean Hackathon Entry by Ronald Stewart

This the MintBean Hackathon I am entering with. It is simple drawing app. It allows to pick your different colors. Furthermore, it will also allow you to pick your canvas background and line Thickness.

The App only uses HTML, SCSS, JavaScript, and Snowpack for development. I wanted to use SCSS for its variables and mixins.

I wanted to use express for the backend. Web sockets for sharing drawings, and web cookies to save the drawings.

My Regional to have the canvas section to overlay overtop the Landing section. The mouse was not aligning with the canvas. I just created a second page for canvas.

I have found out a JavaScript script that align the mouse with the canvas. https://www.geeksforgeeks.org/how-to-draw-with-mouse-in-html-5-canvas/

It did not work with overlay. I decided to use the script and have another HTML page to use canvas.
## My Adding
    * Added Line Colors
    * Added Canvas Background Colors
    * Added Line Thickness
    * Added Clear Canvas
    * Added Touch Support

It is a couple bugs in the app. You can only draw on half of the canvas. The bug us the CSS on the buttons section.
## You Choose of Colors
    • Black
    • Red
    • Yellow
    • White

The landing was difficult today with only sass. It is responsive.

<img alt="Landing Page" src="/images/Screenshot_LandingPage.png" />

I canvas look good in 1920x1080 screens. I have not

Here is the Screen shot of the Canvas HTML Page.

<img alt="Canvas Section" src="/images/canvaspage.png" />
