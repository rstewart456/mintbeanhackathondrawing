let StrokeColor
let LineThickness
const mainMenu = document.querySelector(".menu_icon")
const buttonContainer = document.querySelector(".button_container")
// wait for the content of the window element
// to load, then performs the operations.
// This is considered best practice.
window.addEventListener('load', ()=>{

    lineColor() // Default Line Color to Black
    backgroundColor() // Default Canvas Backround Color to White
    lineThickness() // Default Line Thickness to 10
	resize(); // Resizes the canvas once the window loads

    mainMenu.addEventListener('click', () => {
        visibility = buttonContainer.getAttribute("data-attribute");

        if (visibility === "false") {
            buttonContainer.setAttribute("data-attribute", true)
        } else if (visibility === "true") {
            buttonContainer.setAttribute("data-attribute", false)
        }
    })
    document.getElementById("exit_button").style.backgroundColor = "#3A99FA"
    document.getElementById("clear_button").style.backgroundColor = "#3A99FA"
	document.addEventListener('mousedown', startPainting);
	document.addEventListener('mouseup', stopPainting);
	document.addEventListener('mousemove', sketch);
    document.addEventListener("touchstart", touchstart);
    document.addEventListener("touchmove", touchmove);
    document.addEventListener("touchend", touchend);
	window.addEventListener('resize', resize);
});

const canvas = document.querySelector('#canvas');

// Context for the canvas for 2 dimensional operations
const ctx = canvas.getContext('2d');

// Resizes the canvas to the available size of the window.
function resize(){
ctx.canvas.width = window.innerWidth;
ctx.canvas.height = window.innerHeight;
}

// Stores the initial position of the cursor
let coord = {x:0 , y:0};

// This is the flag that we are going to use to
// trigger drawing
let paint = false;

// Updates the coordianates of the cursor when
// an event e is triggered to the coordinates where
// the said event is triggered.
function getPosition(event){
coord.x = event.clientX - canvas.offsetLeft;
coord.y = event.clientY - canvas.offsetTop;
}

// Function for Touch Control for Mobile Devices.
function touchstart(e) { startPainting(e.touches[0]) }
function touchmove(e) { sketch(e.touches[0]) }
function touchend(e) { stopPainting() }
// The following functions toggle the flag to start
// and stop drawing
function startPainting(event){
paint = true;
getPosition(event);
}
function stopPainting(){
paint = false;
}

function sketch(event){
if (!paint) return;
ctx.beginPath();

ctx.lineWidth = LineThickness;

// Sets the end of the lines drawn
// to a round shape.
ctx.lineCap = 'round';

ctx.strokeStyle = StrokeColor;

// The cursor to start drawing
// moves to this coordinate
ctx.moveTo(coord.x, coord.y);

// The position of the cursor
// gets updated as we move the
// mouse around.
getPosition(event);

// A line is traced from start
// coordinate to this coordinate
ctx.lineTo(coord.x , coord.y);

// Draws the line.
ctx.stroke();
}




function lineColor(e) {
    switch(e) {
        case 'black':
            StrokeColor = e
            document.getElementById("line_color_black").style.backgroundColor = "#FF1313"
            document.getElementById("line_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_white").style.backgroundColor = "#3A99FA"
            break;
        case "red":
            StrokeColor = e
            document.getElementById("line_color_black").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_red").style.backgroundColor = "#FF1313"
            document.getElementById("line_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_white").style.backgroundColor = "#3A99FA"
            break;
        case "yellow":
            StrokeColor = e
            document.getElementById("line_color_black").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_yellow").style.backgroundColor = "#FF1313"
            document.getElementById("line_color_white").style.backgroundColor = "#3A99FA"
            break;
        case "white":
            StrokeColor = e
            document.getElementById("line_color_black").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_white").style.backgroundColor = "#FF1313"
            break;
        default:
            StrokeColor = "black";
            document.getElementById("line_color_black").style.backgroundColor = "#FF1313"
            document.getElementById("line_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("line_color_white").style.backgroundColor = "#3A99FA"
    }

}

function backgroundColor(e) {
    switch(e) {
        case 'black':
            document.getElementById("canvas").style.backgroundColor = e
            document.getElementById("canvas_color_black").style.backgroundColor = "#FF1313"
            document.getElementById("canvas_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_white").style.backgroundColor = "#3A99FA"
            break;
        case "red":
            document.getElementById("canvas").style.backgroundColor = e
            document.getElementById("canvas_color_black").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_red").style.backgroundColor = "#FF1313"
            document.getElementById("canvas_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_white").style.backgroundColor = "#3A99FA"
            break;
        case "yellow":
            document.getElementById("canvas").style.backgroundColor = e
            document.getElementById("canvas_color_black").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_yellow").style.backgroundColor = "#FF1313"
            document.getElementById("canvas_color_white").style.backgroundColor = "#3A99FA"
            break;
        case "white":
            document.getElementById("canvas").style.backgroundColor = e
            document.getElementById("canvas_color_black").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_white").style.backgroundColor = "#FF1313"
            break;
        default:
            document.getElementById("canvas").style.backgroundColor = "white"
            document.getElementById("canvas_color_black").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_red").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_yellow").style.backgroundColor = "#3A99FA"
            document.getElementById("canvas_color_white").style.backgroundColor = "#FF1313"
    }

}

function lineThickness(e) {
    switch(e) {
        case '10':
            LineThickness = e
            document.getElementById("line_thickness_10").style.backgroundColor = "#FF1313"
            document.getElementById("line_thickness_20").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_30").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_40").style.backgroundColor = "#3A99FA"
            break;
        case "20":
            LineThickness = e
            document.getElementById("line_thickness_10").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_20").style.backgroundColor = "#FF1313"
            document.getElementById("line_thickness_30").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_40").style.backgroundColor = "#3A99FA"
            break;
        case "30":
            LineThickness = e
            document.getElementById("line_thickness_10").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_20").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_30").style.backgroundColor = "#FF1313"
            document.getElementById("line_thickness_40").style.backgroundColor = "#3A99FA"
            break;
        case "40":
            LineThickness = e
            document.getElementById("line_thickness_10").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_20").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_30").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_40").style.backgroundColor = "#FF1313"
            break;
        default:
            LineThickness = "10";
            document.getElementById("line_thickness_10").style.backgroundColor = "#FF1313"
            document.getElementById("line_thickness_20").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_30").style.backgroundColor = "#3A99FA"
            document.getElementById("line_thickness_40").style.backgroundColor = "#3A99FA"
    }

}

function clearButton() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}
